import { useState, useEffect, useContext } from 'react';
import { Container, Card, Button, Row, Col, Form } from 'react-bootstrap';
import { useParams, useNavigate, Link } from 'react-router-dom';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';

import React from 'react';

export default function ProductView() {
    const { productId } = useParams();
    const [isActive, setIsActive] = useState(false);
    const { user } = useContext(UserContext);
    const navigate = useNavigate();

    const [name, setName] = useState('');
    const [description, setDescription] = useState('');
    const [price, setPrice] = useState(0);

    const handleCancel = () => {
        navigate(-1); // navigates to the previous page in the history stack
    };

    function update(e) {
        e.preventDefault();
        fetch(`http://localhost:4000/products/${productId}`, {
            method: 'PUT',
            headers: {
                'Content-Type': 'application/json',
                Authorization: `Bearer ${localStorage.getItem('token')}`,
            },
            body: JSON.stringify({
                name: name,
                description: description,
                price: price,
            }),
        })
            .then((res) => {
                res.json();
            })
            .then((data) => {
                if (!data) {
                    setName('');
                    setDescription('');
                    setPrice('');
                    Swal.fire({
                        title: 'Success!',
                        icon: 'success',
                        text: 'The product has been updated!',
                    });
                    navigate('/seeAllProduct');
                } else {
                    Swal.fire({
                        title: 'Something wrong',
                        icon: 'error',
                        text: 'Please try again.',
                    });
                }
            });
    }

    useEffect(() => {
        if (name !== '' && description !== '' && price !== '') {
            setIsActive(true);
        } else {
            setIsActive(false);
        }
    }, [name, description, price]);

    const edited = `/products/${productId}/edit`;
    return (
        <Form onSubmit={(e) => update(e)}>
            <Form.Group controlId="name">
                <Form.Label>Product</Form.Label>
                <Form.Control
                    type="text"
                    placeholder="Product Title"
                    value={name}
                    onChange={(e) => setName(e.target.value)}
                    required
                />
            </Form.Group>

            <Form.Group controlId="description">
                <Form.Label>Description</Form.Label>
                <Form.Control
                    type="text"
                    placeholder="Description"
                    value={description}
                    onChange={(e) => setDescription(e.target.value)}
                    required
                />
            </Form.Group>

            <Form.Group className="mb-3" controlId="price">
                <Form.Label>Price</Form.Label>
                <Form.Control
                    type="number"
                    placeholder="How much?"
                    value={price}
                    onChange={(e) => setPrice(e.target.value)}
                    required
                />
            </Form.Group>

            {isActive ? (
                <>
                    <Button
                        className="mx-2"
                        mxClass="mx-2"
                        variant="primary"
                        type="submit"
                        id="submitBtn"
                    >
                        Update
                    </Button>
                    <Button variant="primary" type="submit" id="submitBtn" onClick={handleCancel}>
                        Cancel
                    </Button>
                </>
            ) : (
                <>
                    <Button className="mx-2" variant="danger" type="submit" id="submitBtn" disabled>
                        Update
                    </Button>
                    <Button variant="primary" type="submit" id="submitBtn" onClick={handleCancel}>
                        Cancel
                    </Button>
                </>
            )}
        </Form>
    );
}
