import { useState, useEffect, useContext } from 'react';
import { Container, Card, Button, Row, Col } from 'react-bootstrap';
import { useParams, Link } from 'react-router-dom';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';

export default function CourseView() {
    const { productId } = useParams();
    const { user } = useContext(UserContext);

    const [name, setName] = useState('');
    const [description, setDescription] = useState('');
    const [price, setPrice] = useState(0);
    const [quantity, setQuantity] = useState(1);

    const product = (productId) => {
        const userId = user.id;
        const productName = name;

        fetch(`http://localhost:4000/users/checkout`, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                Authorization: `Bearer ${localStorage.getItem('token')}`,
            },
            body: JSON.stringify({
                productId: productId,
                userId: userId,
                productName: productName,
                quantity: quantity,
            }),
        })
            .then((res) => res.json())
            .then((data) => {
                console.log(data);

                if (data) {
                    Swal.fire({
                        title: 'Checked Out!',
                        icon: 'success',
                        text: 'Thank you for Purchasing.',
                    });
                } else {
                    Swal.fire({
                        title: 'Something went wrong',
                        icon: 'error',
                        text: 'Please try again.',
                    });
                }
            });
    };

    useEffect(() => {
        console.log(productId);

        fetch(`http://localhost:4000/products/${productId}`)
            .then((res) => res.json())
            .then((data) => {
                console.log(data);

                setName(data.name);
                setDescription(data.description);
                setPrice(data.price);
            });
    }, [productId]);

    const handleIncrement = () => {
        setQuantity(quantity + 1);
    };

    const handleDecrement = () => {
        if (quantity > 1) {
            setQuantity(quantity - 1);
        }
    };

    return (
        <Container className="mt-5">
            <Row>
                <Col lg={{ span: 5, offset: 3 }}>
                    <Card className="my-3">
                        <Card.Body>
                            {user.id !== null ? (
                                <>
                                    <Button
                                        variant="primary"
                                        block
                                        onClick={() => product(productId)}
                                    >
                                        Confirmation
                                    </Button>
                                </>
                            ) : (
                                <Link className="btn btn-danger btn-block" to="/login">
                                    Buy now
                                </Link>
                            )}
                        </Card.Body>
                    </Card>
                </Col>
            </Row>
        </Container>
    );
}
