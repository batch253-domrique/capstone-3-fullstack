// import { useEffect, useState } from 'react';
// import CxShowCard from '../components/CxShowCard';

// import UserContext from '../UserContext';

// export default function Products() {
//     const [users, setUsers] = useState([]);
//     const { user } = useState(UserContext);
//     useEffect(() => {
//         fetch(`${process.env.REACT_APP_API_URL}/${user.id}/userDetails`, {
//             headers: {
//                 Authorization: `Bearer ${localStorage.getItem('token')}`,
//             },
//         })
//             .then((res) => res.json())
//             .then((data) => {
//                 setUsers(
//                     data.map((users) => {
//                         return <CxShowCard key={users.id} users={users} />;
//                     })
//                 );
//             });
//     }, []);

//     return <>{users}</>;
// }

import { useEffect, useState, useContext } from 'react';
import CxShowCard from '../components/CxShowCard';
import UserContext from '../UserContext';

export default function Products() {
    const [users, setUsers] = useState();
    const { user } = useContext(UserContext);

    useEffect(() => {
        fetch(`${process.env.REACT_APP_API_URL}/users/${user.id}/userDetails`, {
            headers: {
                Authorization: `Bearer ${localStorage.getItem('token')}`,
            },
        })
            .then((res) => res.json())
            .then((data) => {
                setUsers(
                    data.map((user) => {
                        return <CxShowCard key={user.id} user={user} />;
                    })
                );
            });
    }, []);
    // useEffect(() => {
    //     fetch(`${process.env.REACT_APP_API_URL}/users/${user.id}/userDetails`, {
    //         method: 'GET',
    //         headers: {
    //             Authorization: `Bearer ${localStorage.getItem('token')}`,
    //         },
    //     })
    //         .then((res) => res.json())
    //         .then((data) => {
    //             console.log(data);

    //             setUser({
    //                 id: data._id,
    //                 isAdmin: data.isAdmin,
    //             });
    //         });
    // }, []);

    return <>{users}</>;
}
