import { useState, useEffect } from 'react';
import { Form, Button, Row, Col, Container } from 'react-bootstrap';
import { useNavigate, Link } from 'react-router-dom';
import Swal from 'sweetalert2';
// import UserContext from '../UserContext';

export default function Createproduct() {
    // const { user } = useContext(UserContext);
    const navigate = useNavigate();

    const [name, setname] = useState('');
    const [description, setDescription] = useState('');
    const [price, setPrice] = useState('');
    const [image, setImage] = useState('');

    const [isActive, setIsActive] = useState(false);

    function addNewProduct(e) {
        e.preventDefault();

        fetch(`${process.env.REACT_APP_API_URL}/products/create`, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                Authorization: `Bearer ${localStorage.getItem('token')}`,
            },
            body: JSON.stringify({
                name: name,
                description: description,
                price: price,
                image: image,
            }),
        })
            .then((res) => res.json())
            .then((data) => {
                console.log(data);
                if (data === true) {
                    // Clear input fields
                    setname('');
                    setDescription('');
                    setPrice('');

                    Swal.fire({
                        title: 'Registration successful',
                        icon: 'success',
                        text: 'Welcome to Zuitt!',
                    });

                    navigate('/adminDashboard');
                } else {
                    Swal.fire({
                        title: 'Something wrong',
                        icon: 'error',
                        text: 'Please try again.',
                    });
                }
            });
    }

    useEffect(() => {
        if (name !== '' && description !== '' && price !== '') {
            setIsActive(true);
        } else {
            setIsActive(false);
        }
    }, [name, description, price]);

    return (
        <>
            <Container fluid className="">
                <Row className="mb-5">
                    <Col>
                        <div
                            style={{
                                backgroundColor: '#E55C0D',
                                color: '#FFA500',
                                paddingTop: '2rem',
                                paddingBottom: '2rem',
                            }}
                        />
                    </Col>
                </Row>
            </Container>
            <Container className="" fluid>
                <Row
                    className="justify-content-center "
                    style={{ top: 0, left: 0, right: 0, bottom: 0 }}
                >
                    <Col md={4} lg={4} sm={10}>
                        <Form
                            onSubmit={(e) => addNewProduct(e)}
                            style={{
                                border: '1px solid #ccc',
                                padding: '20px',
                                boxShadow: '0px 0px 10px rgba(0, 0, 0, 0.2)',
                            }}
                            className="rounded-4  mb-5  pb-5 "
                        >
                            <h1 className="text-center">
                                <span>
                                    <img src="https://res.cloudinary.com/dv3x7qoak/image/upload/v1679811880/carrot_dvb09c.png" />
                                </span>
                                Create Product
                            </h1>
                            <br></br>
                            <Col>
                                <Form.Group className="mb-3" controlId="name">
                                    <Form.Control
                                        type="text"
                                        placeholder="Product Title"
                                        value={name}
                                        onChange={(e) => setname(e.target.value)}
                                        required
                                    />
                                </Form.Group>

                                <Form.Group className="mb-3" controlId="description">
                                    <Form.Control
                                        type="text"
                                        placeholder="Description"
                                        value={description}
                                        onChange={(e) => setDescription(e.target.value)}
                                        required
                                    />
                                </Form.Group>

                                <Form.Group className="mb-3" controlId="price">
                                    <Form.Control
                                        type="price"
                                        placeholder="How much?"
                                        value={price}
                                        onChange={(e) => setPrice(e.target.value)}
                                        required
                                    />
                                </Form.Group>
                                <Form.Group className="mb-3" controlId="price">
                                    <Form.Control
                                        type="text"
                                        placeholder="Image URL?"
                                        value={image}
                                        onChange={(e) => setImage(e.target.value)}
                                        required
                                    />
                                </Form.Group>
                            </Col>
                            <Col>
                                {isActive ? (
                                    <>
                                        <Button
                                            className="mx-2"
                                            mxClass="mx-2"
                                            variant="primary"
                                            type="submit"
                                            id="submitBtn"
                                        >
                                            Create
                                        </Button>
                                        <Button
                                            as={Link}
                                            to="/adminDashboard"
                                            className="mx-2"
                                            variant="primary"
                                            type="submit"
                                            id="submitBtn"
                                        >
                                            Cancel
                                        </Button>
                                    </>
                                ) : (
                                    <>
                                        <Button
                                            className="mx-2"
                                            variant="danger"
                                            type="submit"
                                            id="submitBtn"
                                            disabled
                                        >
                                            Create
                                        </Button>
                                        <Button
                                            as={Link}
                                            to="/adminDashboard"
                                            className="mx-2"
                                            variant="primary"
                                            type="submit"
                                            id="submitBtn"
                                        >
                                            Cancel
                                        </Button>
                                    </>
                                )}
                            </Col>
                        </Form>
                    </Col>
                </Row>
            </Container>
        </>
    );
}
