//NOTE: THIS IS USE TO MANIPULATE THE PRODUCT, ONLY USED BY ADMINS
//IT HAS TWO BUTTONS THE UPDATE, THAT WILL ROUTE TO UPDATE PAGE; AND ACTIVE, THAT WILL CHANGE THE STATUS OF THE PRODUCT EITHER RUNNING OR PAUSED

//IMPORTS ===========================================================
import { useState, useEffect, useContext } from 'react';
import { Container, Card, Button, Row, Col } from 'react-bootstrap';
import { useParams, Link } from 'react-router-dom';
import UserContext from '../UserContext';
import React from 'react';
import Swal from 'sweetalert2';

//EXPORTS ==========================================================
export default function EditView() {
    const [isProductActive, setIsProductActive] = useState(true);
    const { productId } = useParams();
    const { user } = useContext(UserContext);
    const [name, setName] = useState('');
    const [description, setDescription] = useState('');
    const [price, setPrice] = useState(0);
    const [setIsActive] = useState();
    const [image, setImage] = useState('');

    useEffect(() => {
        fetch(`http://localhost:4000/products/${productId}/edit`)
            .then((res) => res.json())
            .then((data) => {
                setName(data.name);
                setDescription(data.description);
                setPrice(data.price);
                setIsActive(data.isActive);
                setImage(data.image);
            });
    }, [productId]);
    console.log(description);
    const update = `/products/${productId}/update`;
    // const disable = `/products/${productId}/archive`;
    const handleClickDeactivate = () => {
        fetch(`http://localhost:4000/products/${productId}/archive`, {
            method: 'PATCH',
            headers: {
                'Content-Type': 'application/json',
                Authorization: `Bearer ${localStorage.getItem('token')}`,
            },
            body: JSON.stringify({
                isActive: !isProductActive,
            }),
        })
            .then((response) => response.json())
            .then((data) => {
                console.log(data);
                setIsProductActive(!isProductActive);
            })
            .then((data) => {
                if (isProductActive) {
                    Swal.fire({
                        title: 'Are you sure?',
                        text: 'Once DEACTIVATED, it will not be accessible for viewing within the catalog.',
                        icon: 'warning',
                        showCancelButton: true,
                        confirmButtonColor: '#3085d6',
                        cancelButtonColor: '#d33',
                        confirmButtonText: 'Yes, Deactivate it!',
                    }).then((willDelete) => {
                        if (willDelete) {
                            Swal.fire('Updated!', 'The product has been deactivated.', 'success');
                        }
                    });
                }
            });
    };
    const handleClickActivate = () => {
        fetch(`http://localhost:4000/products/${productId}/unarchive`, {
            method: 'PATCH',
            headers: {
                'Content-Type': 'application/json',
                Authorization: `Bearer ${localStorage.getItem('token')}`,
            },
            body: JSON.stringify({
                isActive: !isProductActive,
            }),
        })
            .then((response) => response.json())
            .then((data) => {
                console.log(data);
                setIsProductActive(!isProductActive);
            })
            .then((data) => {
                if (!isProductActive) {
                    Swal.fire({
                        title: 'Are you sure?',
                        text: 'Once ACTIVATED, the product will be available for browsing within the catalog.',
                        icon: 'warning',
                        showCancelButton: true,
                        confirmButtonColor: '#3085d6',
                        cancelButtonColor: '#d33',
                        confirmButtonText: 'Yes, Activate it!',
                    }).then((result) => {
                        if (result.isConfirmed) {
                            Swal.fire('Updated!', 'The product has been activated.', 'success');
                        }
                    });
                }
            });
    };
    return (
        <Container className="mt-5">
            <Row>
                <Col>
                    <Card className="my-3">
                        <Card.Body>
                            <Card.Img src={image} className="img-fluid" />
                            <Card.Title>{name}</Card.Title>
                            <Card.Subtitle>Description:</Card.Subtitle>
                            <Card.Text>{description}</Card.Text>
                            <Card.Subtitle>Price:</Card.Subtitle>
                            <Card.Text>Php {price}</Card.Text>
                            <Card.Subtitle>Status</Card.Subtitle>
                            {isProductActive === true ? (
                                <Card.Text>The product is currently active and running.</Card.Text>
                            ) : (
                                <Card.Text>The product is inactive and unavailable. </Card.Text>
                            )}

                            {user.isAdmin === true ? (
                                <>
                                    <Button
                                        className="mx-1"
                                        variant="primary"
                                        as={Link}
                                        to={update}
                                    >
                                        Update
                                    </Button>
                                    {!isProductActive && (
                                        <Button variant="success" onClick={handleClickActivate}>
                                            Activate
                                        </Button>
                                    )}

                                    {isProductActive && (
                                        <Button variant="dark" onClick={handleClickDeactivate}>
                                            Deactivate
                                        </Button>
                                    )}
                                    <Button
                                        className="mx-1"
                                        variant="primary"
                                        as={Link}
                                        to="/seeAllProduct"
                                    >
                                        Back
                                    </Button>
                                </>
                            ) : (
                                <>
                                    <Container className="d-flex justify-content-center ">
                                        <Row className="">
                                            <Col xs={12} sm={12} md={6}>
                                                <Button
                                                    className="mr-2"
                                                    variant="primary"
                                                    as={Link}
                                                    to="/cxProduct"
                                                >
                                                    Back
                                                </Button>
                                            </Col>
                                            <Col xs={12} sm={12} md={6}>
                                                <Button variant="primary" as={Link} to="/cxProduct">
                                                    Checkout
                                                </Button>
                                            </Col>
                                        </Row>
                                    </Container>
                                </>
                            )}
                        </Card.Body>
                    </Card>
                </Col>
            </Row>
        </Container>
    );
}
