//NOTES: IT IS USE TO GIVE AN OPTION TO ADMIN WHETHER WANT TO:
//A.) SEE ALL PRODUCT - TO RETRIEVE ALL PRODUCTS ; UPDATE PRODUCT ; ENABLE AND DISABLE PRODUCT
//B.) CREATE PRODUCT - TO ADD NEW PRODUCT
//C.) SHOW CUSTOMER'S ORDERS - TO SEE ALL ORDERS OF CUSTOMERS; RETRIEVE CUSTOMER'S DETAILS

//IMPORTS============================================================
import { Row, Col, Card, Button, Container } from 'react-bootstrap';
import { useState, useEffect, useContext } from 'react';
import { Link } from 'react-router-dom';

//EXPORTS============================================================
export default function AdminOptions() {
    const [isHovered, setIsHovered] = useState(false);
    return (
        <>
            <Container fluid>
                <Row className="mb-sm-5">
                    <Col>
                        <div
                            style={{
                                backgroundColor: '#E55C0D',
                                color: '#FFA500',
                                paddingTop: '2rem',
                                paddingBottom: '2rem',
                            }}
                        />
                    </Col>
                </Row>
            </Container>

            <Row className="mt-3 mt-md-5 pt-md-5 mt-lg-5 pt-lg-5 mb-3 justify-content-center ">
                <Col xs={10} md={3} className="my-3 mx-md-5 mx-lg-4">
                    <Card
                        className="cardHighlight"
                        style={{
                            boxShadow: '0px 4px 8px rgba(0, 0, 0, 0.1)',
                            transition: 'transform 0.2s ease-in-out',
                            transform: 'scale(1)',
                        }}
                        onMouseOver={(e) => {
                            e.currentTarget.style.transform = 'scale(1.06)';
                        }}
                        onMouseOut={(e) => {
                            e.currentTarget.style.transform = 'scale(1)';
                        }}
                    >
                        <Card.Body as={Link} to="/seeAllProduct">
                            <Card.Img src="https://res.cloudinary.com/dv3x7qoak/image/upload/v1679830327/1_j0lclr.png"></Card.Img>
                            <Card.Title>
                                <Button variant="primary" className="w-100">
                                    <h2>SEE ALL PRODUCT</h2>
                                </Button>
                            </Card.Title>
                        </Card.Body>
                    </Card>
                </Col>
                <Col xs={10} md={3} className="my-3 mx-md-5 mx-lg-4">
                    <Card
                        className="cardHighlight"
                        style={{
                            boxShadow: '0px 4px 8px rgba(0, 0, 0, 0.1)',
                            transition: 'transform 0.2s ease-in-out',
                            transform: 'scale(1)',
                        }}
                        onMouseOver={(e) => {
                            e.currentTarget.style.transform = 'scale(1.06)';
                        }}
                        onMouseOut={(e) => {
                            e.currentTarget.style.transform = 'scale(1)';
                        }}
                    >
                        <Card.Body as={Link} to="/createProduct">
                            <Card.Img src="https://res.cloudinary.com/dv3x7qoak/image/upload/v1679830327/2_ydhuyo.png"></Card.Img>
                            <Card.Title>
                                <Button variant="primary" className="w-100">
                                    <h2>CREATE PRODUCT</h2>
                                </Button>
                            </Card.Title>
                        </Card.Body>
                    </Card>
                </Col>
                <Col xs={10} md={3} className="my-3 mx-md-5 mx-lg-4">
                    <Card
                        className="cardHighlight"
                        style={{
                            boxShadow: '0px 4px 8px rgba(0, 0, 0, 0.1)',
                            transition: 'transform 0.2s ease-in-out',
                            transform: 'scale(1)',
                        }}
                        onMouseOver={(e) => {
                            e.currentTarget.style.transform = 'scale(1.06)';
                        }}
                        onMouseOut={(e) => {
                            e.currentTarget.style.transform = 'scale(1)';
                        }}
                    >
                        <Card.Body as={Link} to="/none">
                            <Card.Img src="https://res.cloudinary.com/dv3x7qoak/image/upload/v1679830327/3_y1hr1y.png"></Card.Img>
                            <Button variant="primary" className="w-100">
                                <h2>CUSTOMER'S ORDER</h2>
                            </Button>
                        </Card.Body>
                    </Card>
                </Col>
            </Row>
        </>
    );
}
