//NOTES THIS IS ONLY USE FOR ADDING TITLE HEAD IN A PAGE IN GETTING ACTIVE PRODUCTS
//FOR TITLE PURPOSE ONLY

import { Row, Col } from 'react-bootstrap';

export default function GetActive() {
    return (
        <Row>
            <Col>
                <h1>List of Active Products</h1>
            </Col>
        </Row>
    );
}
