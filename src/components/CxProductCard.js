//NOTE: THIS IS FOR CUSTOMER'S EYE ONLY
//WHEN YOU CLICK THE SHOPNOW IN THE NAVBAR, IT WILL ROUTE HERE.
//THIS IS WHERE CX CAN ADD TO CART AND THEN WILL ROUTE TO CHECKOUT ONCE HITTING THE "ADD TO CART BUTTON"

import { Link } from 'react-router-dom';
import { useEffect, useState } from 'react';
import { Container, Row, Col, Form, Button, Card } from 'react-bootstrap';

export default function ProductCard({ product }) {
    const { _id, name, description, price, image } = product;

    //==========
    /* function Product({ name, price, onAddToCart }) {
        const [quantity, setQuantity] = useState(1);

        const handleQuantityChange = (event) => {
            setQuantity(parseInt(event.target.value));
        };

        const handleAddToCartClick = () => {
            onAddToCart(name, price, quantity);
        };

        return (
            <div>
                <h3>{name}</h3>
                <p>${price.toFixed(2)}</p>
                <Form.Group>
                    <Form.Label htmlFor="quantity">Quantity:</Form.Label>
                    <Form.Control
                        type="number"
                        id="quantity"
                        name="quantity"
                        min="1"
                        value={quantity}
                        onChange={handleQuantityChange}
                    />
                </Form.Group>
                <Button onClick={handleAddToCartClick} variant="primary">
                    Add to Cart
                </Button>
            </div>
        );
    }

    function Cart({ items, onRemoveFromCart }) {
        const getTotalPrice = () => {
            return items.reduce((total, item) => total + item.price * item.quantity, 0);
        };

        const handleRemoveFromCartClick = (item) => {
            onRemoveFromCart(item);
        };

        return (
            <div>
                <h3>Shopping Cart</h3>
                {items.map((item, index) => (
                    <div key={index}>
                        <p>
                            {item.name} ({item.quantity}) - ${item.price.toFixed(2)} each
                        </p>
                        <Button onClick={() => handleRemoveFromCartClick(item)} variant="danger">
                            Remove
                        </Button>
                    </div>
                ))}
                <p>Total: ${getTotalPrice().toFixed(2)}</p>
            </div>
        );
    }
    const [cartItems, setCartItems] = useState([]);

    const handleAddToCart = (name, price, quantity) => {
        const itemIndex = cartItems.findIndex((item) => item.name === name);
        if (itemIndex === -1) {
            // Item is not yet in the cart
            setCartItems([...cartItems, { name, price, quantity }]);
        } else {
            // Item is already in the cart
            const updatedCartItems = [...cartItems];
            updatedCartItems[itemIndex].quantity += quantity;
            setCartItems(updatedCartItems);
        }
    };

    const handleRemoveFromCart = (item) => {
        const updatedCartItems = cartItems.filter((cartItem) => cartItem !== item);
        setCartItems(updatedCartItems);
    }; */
    //==========

    return (
        <>
            {/*  <Container fluid>
                <Row className="justify-content-center">
                    <Col md={6}>
                        <h1>Shopping Cart Example</h1>
                        <Product name="Product 1" price={9.99} onAddToCart={handleAddToCart} />
                        <Product name="Product 2" price={14.99} onAddToCart={handleAddToCart} />
                    </Col>
                    <Col md={6}>
                        <Cart items={cartItems} onRemoveFromCart={handleRemoveFromCart} />
                    </Col>
                </Row>
            </Container>
 */}
            <Col xs={12} md={3} className="mt-3 mb-3">
                <Card
                    className="cardHighlight"
                    style={{
                        boxShadow: '0px 4px 8px rgba(0, 0, 0, 0.1)',
                        transition: 'transform 0.2s ease-in-out',
                        transform: 'scale(1)',
                    }}
                    onMouseOver={(e) => {
                        e.currentTarget.style.transform = 'scale(1.06)';
                    }}
                    onMouseOut={(e) => {
                        e.currentTarget.style.transform = 'scale(1)';
                    }}
                >
                    <Card.Body as={Link} to={`/products/${_id}`} style={{ textDecoration: 'none' }}>
                        <Col>
                            <Card.Img src={image} className="img-fluid" />
                            <Card.Title>
                                <h5 className="product-names">{name}</h5>
                            </Card.Title>
                            <Card.Text>{description}</Card.Text>
                            <Card.Text className="price-color">
                                <p>
                                    <span>&#8369;</span> {price}
                                </p>
                            </Card.Text>
                        </Col>
                    </Card.Body>
                </Card>
            </Col>
        </>
    );
}
