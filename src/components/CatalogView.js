//NOTES: THIS PAGE IS FOR ADMIN'S EYE ONLY
//IT IS A COMPONENT INSIDE THE CATALOG CARD, WHEN YOU HIT THE "DETAILS" IT SHOWS THE DETAILS OF THE PRODUCT BUT CAN NEVER EDIT IT

import { useState, useEffect, useContext } from 'react';
import { Container, Card, Button, Row, Col } from 'react-bootstrap';
import { useParams, useNavigate, Link } from 'react-router-dom';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';

import React from 'react';

export default function ProductView() {
    // The "useParams" hook allows us to retrieve the productId passed via the URL
    const { productId } = useParams();

    const { user } = useContext(UserContext);

    // Allows us to gain access to methods that will allow us to redirect a user to a different page after enrolling a product
    //an object with methods to redirect the user
    const navigate = useNavigate();

    const [name, setName] = useState('');
    const [description, setDescription] = useState('');
    const [price, setPrice] = useState(0);

    useEffect(() => {
        fetch(`http://localhost:4000/products/${productId}`)
            .then((res) => res.json())
            .then((data) => {
                setName(data.name);
                setDescription(data.description);
                setPrice(data.price);
            });
    }, [productId]);

    return (
        <Container className="mt-5">
            <Row>
                <Col>
                    <Card className="my-3">
                        <Card.Body>
                            <Card.Title>{name}</Card.Title>
                            <Card.Subtitle>Description:</Card.Subtitle>
                            <Card.Text>{description}</Card.Text>
                            <Card.Subtitle>Price:</Card.Subtitle>
                            <Card.Text>Php {price}</Card.Text>
                            {/* <Card.Subtitle>MAGPAKITA Sana</Card.Subtitle>
                            <Card.Text>8 am - 5 pm</Card.Text> */}

                            {user.isAdmin === true ? (
                                <Button variant="primary" as={Link} to="/catalog">
                                    Back
                                </Button>
                            ) : (
                                <>
                                    <Container className="d-flex justify-content-center ">
                                        <Row className="">
                                            <Col xs={12} sm={12} md={6}>
                                                <Button
                                                    className="mr-2"
                                                    variant="primary"
                                                    as={Link}
                                                    to="/cxProduct"
                                                >
                                                    Back
                                                </Button>
                                            </Col>
                                            <Col xs={12} sm={12} md={6}>
                                                <Button variant="primary" as={Link} to="/cxProduct">
                                                    Checkout
                                                </Button>
                                            </Col>
                                        </Row>
                                    </Container>
                                </>
                            )}
                        </Card.Body>
                    </Card>
                </Col>
            </Row>
        </Container>
    );
}
