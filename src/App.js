//GENERALS================================================
import { useState, useEffect } from 'react';
import { BrowserRouter as Router } from 'react-router-dom';
import { Route, Routes } from 'react-router-dom';
import './App.css';

//COMPONENTS================================================
import AppNavbar from './components/AppNavbar';
import CatalogView from './components/CatalogView';
import EditView from './components/EditView';
import ProductView from './components/ProductView';
import { UserProvider } from './UserContext';

//PAGES================================================
import A_SuperMain from './pages/A_SuperMain';
import AdminDashboard from './pages/AdminDashboard';
import AddToCartFE from './pages/AddToCartFE';
import Catalog from './pages/Catalog';
import Checkout from './pages/Checkout';
import Createproduct from './pages/Createproduct';
import CxMain from './pages/CxMain';
import CxProduct from './pages/CxProduct';
import CxShowOrders from './pages/CxShowOrders';
import Error from './pages/Error';
import Login from './pages/Login';
import Logout from './pages/Logout';
import Register from './pages/Register';
import SeeAllProduct from './pages/SeeAllProduct';
import Update from './pages/Update';

function App() {
    const [user, setUser] = useState({
        id: null,
        isAdmin: null,
    });

    const unsetUser = () => {
        localStorage.clear();
    };

    useEffect(() => {
        fetch(`${process.env.REACT_APP_API_URL}/users/${user.id}/userDetails`, {
            method: 'GET',
            headers: {
                Authorization: `Bearer ${localStorage.getItem('token')}`,
            },
        })
            .then((res) => res.json())
            .then((data) => {
                console.log(data);

                setUser({
                    id: data._id,
                    isAdmin: data.isAdmin,
                });
            });
    }, []);

    return (
        <>
            <UserProvider value={{ user, setUser, unsetUser }}>
                <Router>
                    <AppNavbar />

                    <Routes style={{ position: 'relative' }}>
                        <Route path="/" element={<A_SuperMain />} />
                        <Route path="/adminDashboard" element={<AdminDashboard />} />
                        <Route path="/addToCartFE" element={<AddToCartFE />} />
                        <Route path="/catalog" element={<Catalog />} />
                        <Route path="/products/:productId/catalogView" element={<CatalogView />} />
                        <Route path="/checkout" element={<Checkout />} />
                        <Route path="/createProduct" element={<Createproduct />} />
                        <Route path="/cxMain" element={<CxMain />} />
                        <Route path="/cxShowOrder" element={<CxShowOrders />} />
                        <Route path="/cxProduct" element={<CxProduct />} />
                        <Route path="/products/:productId/edit" element={<EditView />} />
                        <Route path="*" element={<Error />} />
                        <Route path="/login" element={<Login />} />
                        <Route path="/logout" element={<Logout />} />
                        <Route path="/products/:productId" element={<ProductView />} />
                        <Route path="/register" element={<Register />} />
                        <Route path="/seeAllProduct" element={<SeeAllProduct />} />
                        <Route path="/products/:productId/update" element={<Update />} />
                    </Routes>
                </Router>
            </UserProvider>
        </>
    );
}

export default App;
